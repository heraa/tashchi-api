<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert(
            [
                'name' => 'admin',
                'account' => 'heraa',
                'email' => 'xjaqil@163.com',
                'password' => Hash::make('a38a1d428e2be36118a4612c0120b3b7'),
                'remember_token' => Str::random(10),
                'created_at' => Carbon::now()->timestamp,
                'updated_at' => Carbon::now()->timestamp
            ]
        );
    }
}
