<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookkeepingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookkeeping', function (Blueprint $table) {
            $table->id();
            $table->integer('uid')->comment('用户ID');
            $table->tinyInteger('cid')->comment('类型ID');
            $table->decimal('money',10,2)->comment('金额');
            $table->text('remarks')->comment('备注');
            $table->integer('date')->comment('日期');
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookkeeping');
    }
}
