<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stuns', function (Blueprint $table) {
            $table->id();
            $table->decimal('price',10,2)->comment('金额');
            $table->integer('stun_count')->comment('数量');
            $table->integer('price_image')->comment('图片');
            $table->integer('borrow_flag')->comment('是否借');
            $table->integer('date_time')->comment('买卖时间');
            $table->tinyInteger('status')->comment('状态');
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stuns');
    }
}
