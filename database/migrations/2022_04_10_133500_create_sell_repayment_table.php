<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellRepaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_repayment', function (Blueprint $table) {
            $table->id();
            $table->decimal('repayment_price',10,2)->comment('还款');
            $table->integer('pid')->comment('玉石ID');
            $table->integer('date_time')->comment('还款时间');
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_repayment');
    }
}
