<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Sell
 *
 * @property int $id
 * @property int $pid
 * @property string $sell_price
 * @property int $date_time
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Models\Stuns|null $stuns
 * @method static \Illuminate\Database\Eloquent\Builder|Sell newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sell newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sell query()
 * @method static \Illuminate\Database\Eloquent\Builder|Sell whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sell whereDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sell whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sell wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sell whereSellPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sell whereUpdatedAt($value)
 */
	class Sell extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StoneRepayment
 *
 * @property int $id
 * @property int $pid
 * @property string $repayment_price
 * @property int $date_time
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|StoneRepayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StoneRepayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StoneRepayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|StoneRepayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StoneRepayment whereDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StoneRepayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StoneRepayment wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StoneRepayment whereRepaymentPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StoneRepayment whereUpdatedAt($value)
 */
	class StoneRepayment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Stuns
 *
 * @property int $id 玉id
 * @property int $u_id 用户ID
 * @property string $price 玉价格
 * @property string|null $sell_price
 * @property int $repayment_price
 * @property string|null $buy_name
 * @property int $stun_count 玉数量
 * @property string $price_image 玉图片
 * @property int|null $borrow_flag 借入 1：nisiy amas 2:nisy
 * @property int|null $lend_flag 借出 1:kerz ams 2:qerz
 * @property int $status 状态
 * @property string|null $text
 * @property int $date_time
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property-read \App\Models\Sell|null $sell
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns query()
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns user()
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereBorrowFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereBuyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereLendFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns wherePriceImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereRepaymentPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereSellPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereStunCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereUId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stuns whereUpdatedAt($value)
 */
	class Stuns extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $account
 * @property string $name
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

