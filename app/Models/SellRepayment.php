<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class SellRepayment extends Model
{
    protected $table = 'sell_repayment';
    protected $dateFormat = "U";
    protected $fillable = ['id', 'repayment_price', 'pid','date_time'];

    public function setDateTimeAttribute($value)
    {
        $this->attributes['date_time'] = is_int($value) ? $value : strtotime($value);
    }

    public function getDateTimeAttribute()
    {
        return date('Y-m-d', $this->attributes['date_time']);
    }
    public function stuns()
    {
        return $this->belongsTo(Stuns::class,'pid','id');
    }



}
