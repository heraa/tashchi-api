<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BuyRepayment extends Model
{
    protected $table = 'buy_repayment';
    protected $dateFormat = "U";
    protected $fillable = ['id','pid', 'repayment_price', 'date_time'];

    public function setDateTimeAttribute($value)
    {
        $this->attributes['date_time'] = is_int($value) ? $value : strtotime($value);
    }

    public function getDateTimeAttribute()
    {
        return date('Y-m-d', $this->attributes['date_time']);
    }



}
