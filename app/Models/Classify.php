<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Classify extends Model
{
    use HasFactory;

    protected $table = 'classify';
    protected $dateFormat = "U";

    protected $fillable = ['id', 'name', 'money'];
    protected $hidden = ['uid'];

    public function scopeUser($query)
    {
        $id = Auth::id();
        $query->where('uid');
    }

    public function setDateTimeAttribute($value)
    {
        $this->attributes['date_time'] = is_int($value) ? $value : strtotime($value);
    }

    public function getDateTimeAttribute()
    {
        return date('Y-m-d', $this->attributes['date_time']);
    }

    public function bookkeeping()
    {
        return $this->hasMany(Bookkeeping::class, 'cid', 'id');
    }
}

