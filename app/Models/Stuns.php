<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Stuns extends Model
{
    protected $table = 'stuns';
    protected $dateFormat = "U";

    protected $fillable = ['id','uid', 'price', 'stun_count','sell_date', 'price_image', 'borrow_flag','date_time', 'status'];
    protected $hidden = ['uid'];

    public function getPriceImageAttribute($key)
    {
         $path = unserialize($key);
        return get_file_url($path);
    }

    public function scopeUser($query)
    {
        $id = Auth::id();
        $query->where('uid', $id);
    }
    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function setPriceImageAttribute($key)
    {
        return $this->attributes['price_image'] = serialize($key);
    }

    public function sell()
    {
        return $this->hasMany(SellRepayment::class, 'pid', 'id');
    }
    public function buy()
    {
        return $this->hasMany(BuyRepayment::class, 'pid', 'id');
    }

    public function setDateTimeAttribute($value)
    {
        $this->attributes['date_time'] = is_int($value) ? $value : strtotime($value);
    }

    public function setSellDateAttribute($value)
    {
        $this->attributes['sell_date'] = is_int($value) ? $value : strtotime($value);
    }

//    public function getDateTimeAttribute()
//    {
//        return date('Y-m-d', $this->attributes['date_time']);
//    }
}
