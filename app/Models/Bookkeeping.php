<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Bookkeeping extends Model
{
    use HasFactory;
    protected $table = 'bookkeeping';
    protected $dateFormat = "U";

    protected $fillable = ['id','uid','cid','money','remarks','date'];
    protected $hidden = ['uid'];

    public function scopeUser($query)
    {
        $id = Auth::id();
        $query->where('uid', $id);
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = is_int($value) ? $value : strtotime($value);
    }

    public function getDateAttribute()
    {
        return date('Y-m-d', $this->attributes['date']);
    }
    public function classify(){
        return $this->hasOne(Classify::class,'id','cid');
    }

}
