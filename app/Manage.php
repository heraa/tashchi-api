<?php

namespace App;

use Illuminate\Support\Facades\Auth;

class Manage
{

    public static function getId()
    {
        try {
            return Manage::user()->id;
        } catch (\Exception $e) {

        }

    }

    /**
     * Get current login user.
     *
     * @return mixed
     */
    public static function user()
    {
        if (Auth::guard('sanctum')->check()) {
            return Auth::guard('sanctum')->user();
        }
    }





}
