<?php
/**
 * Created by PhpStorm.
 * User: ablimit
 * Date: 2017/9/21
 * Time: 上午10:57
 */

namespace App\Repositories;

use App\Models\Stuns;
use Carbon\Carbon;

class StunsRepository
{

    protected $model;

    /**
     *
     * @var Stuns $adv
     */
    public function __construct(Stuns $stuns)
    {

        $this->model = $stuns;
    }


    public function advert($position_id, $enabled = 1)
    {
        return $this->model->select(['ad_id', 'media_type', 'ad_code', 'ad_link'])
            ->where(['position_id' => $position_id, 'enabled' => $enabled])
            ->orderby('ad_id', 'ASC')
            ->get();
    }

    public function stuns_list($startDate, $endDate)
    {
        return $this->model->whereBetween('date_time', [$startDate, $endDate])->orWhereBetween('sell_date', [$startDate, $endDate])->get();
    }

    protected function stuns_date_sum($data, $startDate, $endDate)
    {

        $sell = $data->whereBetween('sell_date', [$startDate, $endDate]);
        $buy = $data->whereBetween('date_time', [$startDate, $endDate]);
        $filters = $buy->concat($sell);
        return $filters;

    }

    public function statistic_stuns_format_data($start_date, $end_date, $data, $type): array
    {
        $new_data = [];
        if ($type === 'week') {
            for ($i = 0; $i <= 6; $i++) {
                $new_data = $this->getNew_data($start_date, $i, $data, $new_data, $type);
            }
        } elseif ($type === 'month') {
            $total_day = Carbon::createFromTimestamp($start_date)->daysInMonth;
            for ($i = 0; $i <= $total_day - 1; $i++) {
                $new_data = $this->getNew_data($start_date, $i, $data, $new_data, $type);
            }
        } else {
            for ($i = 0; $i <= 11; $i++) {
                $new_data = $this->getNew_data($start_date, $i, $data, $new_data, $type);
            }
        }
        return $new_data;
    }

    /**
     * @param $start_date
     * @param int $i
     * @param $data
     * @param string $type
     * @param array $new_data
     * @return array
     */
    protected function getNew_data($start_date, int $i, $data, array $new_data, string $type): array
    {
        $ext = '';
        $date = Carbon::createFromTimeStamp($start_date)->addDays($i);
        $startTimestamp = $date->startOfDay()->timestamp;
        $endTimestamp = $date->endOfDay()->timestamp;
        $day = $date->day;
        switch ($type) {
            case 'month':
            case 'week':
                $ext = '日';
                break;
            case 'year':
                $ext = '月';
                $date = Carbon::createFromTimeStamp($start_date)->addMonth($i);
                $day = $date->month;
                $startTimestamp = $date->startOfMonth()->timestamp;
                $endTimestamp = $date->endOfMonth()->timestamp;
                break;
        }
        $buy_price = $data->whereBetween('date_time', [$startTimestamp, $endTimestamp])->sum('price');
        $sell_price = $data->whereBetween('sell_date', [$startTimestamp, $endTimestamp])->sum('sell_price');
        $new_data[] = [
            'date' => $day . $ext,
            'type' => '买玉',
            'value' => $buy_price
        ];
        $new_data[] = [
            'date' => $day . $ext,
            'type' => '卖玉',
            'value' => $sell_price
        ];
        return $new_data;
    }


}
