<?php
/**
 * Created by PhpStorm.
 * User: 阿卜杜赛麦提
 * Date: 2019-03-21
 * Time: 12:58
 */
/**
 * 玉石状态
 * 1:买玉
 * 2:卖玉
 * 3:代理玉
 */
const STONE_STATUS_BUY = 1;
const STONE_STATUS_SELL = 2;
//const STONE_STATUS_ACTING = 3;


/**
 * 借入：借出
 * 借入：1:nisiy amas 2:nisy
 * 借出:1:kerz ams 2:qerz
 */
const BORROW_FLAG_FALSE = 1;
const BORROW_FLAG_TRUE = 2;
const LEND_FLAG_FALSE = 1;
const LEND_FLAG_TRUE = 3;

/**
 * 提示
 */
const TIPS_STONE_FAILED_SELL = '给钱大于卖价!';
const TIPS_STONE_FAILED_BUY = '给钱大于买价!';



