<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api;
use App\Manage;

class UserController extends Controller
{
    use z;

    public function currentUser(){
        $user  = Manage::user();
        return  $this->success($user);
    }
}
