<?php

namespace App\Http\Controllers;

use App\Http\Requests\StunsRequests;
use App\Models\BuyRepayment;
use App\Models\Stuns;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StunsController extends Controller
{
    use ApiResponse;

    private $stuns, $buy_repayment, $paginate, $image_path;

    public function __construct(Stuns $stuns, BuyRepayment $buyRepayment)
    {
        $this->stuns = $stuns;
        $this->buy_repayment = $buyRepayment;
        $this->paginate = 10;
        $this->image_path = 'stuns/images';



    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->all();

        $query = $this->stuns->user()->where('status', '=', $params['status']);
        //价格查询
        if ($request->has('min_price') && $request->has('max_price')) {
            $min_price = $params['min_price'];
            $max_price = $params['max_price'];
            if ($min_price > 0) {
                $query = $query->where('price', '>=', $min_price);
            }
            if ((!$max_price) <= 0) {
                $query = $query->where('price', '>=', $min_price)->where('price', '<=', $max_price);
            }
        }
        //分页
        if ($request->has('pageSize')) {
            $this->paginate = $params['pageSize'];
        }
        $data = $query->paginate($this->paginate);
        return $this->success($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StunsRequests $request)
    {
        $uid = Auth::id();
        $data = $request->all();
        $repayment_price = $data['repayment_price'];
        $price = $data['price'];
        $data['uid'] = $uid;
        $equal = $price - $repayment_price;
        if ($equal < 0) {
            return $this->failed(TIPS_STONE_FAILED_SELL, 422);
        }
        $stone_data = $this->stuns->create($data);
        if ($stone_data) {
            $stone_data = [
                'pid' => $stone_data->id,
                'repayment_price' => $repayment_price,
                'date_time' => $stone_data->date_time
            ];
            if ($this->buy_repayment->create($stone_data))
                return $this->message('添加成功！！');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->stuns->with(['buy', 'sell'])
            ->withSum('buy', 'repayment_price')
            ->withSum('sell', 'repayment_price')
            ->find($id);
        return $this->success($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->stuns->withSum('buy', 'repayment_price')->withSum('sell', 'repayment_price')->find($id);

        if (count($data->sell) > 0) {
            return $this->failed('先收款记录删除！',422);
        }
        if (count($data->buy) > 0) {
            return $this->failed('先还款记录删除！',422);
        }
        if ($this->stuns->destroy($id)) {
            return $this->message('删除成功！');
        }
    }

    public function upload(Request $request)
    {
        $data = upload($request, $this->image_path);
        return $this->success($data);
    }
}
