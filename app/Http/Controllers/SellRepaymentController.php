<?php

namespace App\Http\Controllers;

use App\Http\Requests\SellRequests;
use App\Models\SellRepayment;
use App\Models\Stuns;
use Illuminate\Http\Request;

class SellRepaymentController extends Controller
{

    use ApiResponse;

    private $sell_repayment, $stuns, $paginate;

    public function __construct(SellRepayment $sellRepayment, Stuns $stuns)
    {
        $this->sell_repayment = $sellRepayment;
        $this->stuns = $stuns;
        $this->paginate = 10;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SellRequests $request)
    {
        $request = $request->all();
        $stone = $this->stuns->find($request['pid']);
        if ($stone->status === STONE_STATUS_BUY) {
            $arrears = $request['sell_price'] - $request['repayment_price'];
        } else {
            //总收款
            $sell_debt = $this->sell_repayment->where('pid', '=', $request['pid'])->sum('repayment_price');
            //欠款
            $arrears = $stone->sell_price - ($sell_debt + $request['repayment_price']);
        }
        if ($arrears < 0) {
            return $this->failed(TIPS_STONE_FAILED_SELL, 422);
        }
        $data = [
            'repayment_price' => $request['repayment_price'],
            'pid' => $request['pid'],
            'date_time' => $request['date_time'],
        ];
        if ($this->sell_repayment->create($data)) {
            if ($stone->status === STONE_STATUS_BUY) {
                $stone->sell_price = $request['sell_price'];
                $stone->sell_date = $request['date_time'];
                $stone->buy_name = $request['buy_name'];
                $stone->status = STONE_STATUS_SELL;
                $stone->save();
            }
            return $this->message('操作成功!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sell = $this->sell_repayment->find($id);
        $list = $this->sell_repayment->where('pid', '=', $sell->pid)->get();

        if ($this->sell_repayment->destroy($id)) {
            if (count($list) <= 1) {
                $stuns = $this->stuns->find($sell->pid);
                $stuns->buy_name = '';
                $stuns->sell_price = 0;
                $stuns->status = STONE_STATUS_BUY;
                $stuns->save();
            }
            return $this->message('删除成功！');
        }
    }
}
