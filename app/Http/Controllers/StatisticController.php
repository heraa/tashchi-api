<?php

namespace App\Http\Controllers;

use App\Http\Requests\StunsRequests;
use App\Models\Bookkeeping;
use App\Models\BuyRepayment;
use App\Models\Classify;
use App\Models\Stuns;
use App\Repositories\StunsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    use ApiResponse;

    private $stuns, $buy_repayment, $paginate, $image_path,$stunsRepositories;

    public function __construct(Stuns $stuns, BuyRepayment $buyRepayment,StunsRepository $stunsRepositories,Classify $classify)
    {
        $this->stuns = $stuns;
        $this->classify = $classify;
        $this->stunsRepositories = $stunsRepositories;
        $this->buy_repayment = $buyRepayment;
        $this->paginate = 10;
    }


    public function stuns_total()
    {
        //总价
        $total_price = $this->stuns->sum('price');
        //总卖价
        $total_sell_price = $this->stuns->where('status', '=', STONE_STATUS_SELL)->sum('sell_price');
        //总买价
        $total_buy_price = $this->stuns->where('status', '=', STONE_STATUS_SELL)->sum('price');
        //收入
        $income = $total_sell_price - $total_buy_price;
        //总未卖价
        $total_not_price = $this->stuns->where('status', '=', STONE_STATUS_BUY)->sum('price');
        //已卖数
        $total_sell_count = $this->stuns->where('status', '=', STONE_STATUS_SELL)->sum('stun_count');
        //未卖数
        $total_not_sell_count = $this->stuns->where('status', '=', STONE_STATUS_BUY)->sum('stun_count');
        //总已卖价
        $sell_price = $this->stuns->sum('sell_price');
        //收入
        $total_sell = $this->stuns->withSum('sell', 'repayment_price')->withSum('buy', 'repayment_price')->get();
        //总还款金额
        $total_buy_price = 0;
        //总收款金额
        $total_sell_price = 0;
        foreach ($total_sell as $item) {
            $total_sell_price += $item->sell_sum_repayment_price;
            $total_buy_price += $item->buy_sum_repayment_price;
        }
        //总欠款
        $total_debt = $total_price - $total_buy_price;
        //还没收款金额
        $total_balance = $sell_price - $total_sell_price;
        return $this->success(
            [
                'income' => $income,
                'total_price' => $total_price,
                'total_not_price' => $total_not_price,
                'sell_price' => $sell_price,
                'total_buy_price' => $total_buy_price,
                'total_sell_price' => $total_sell_price,
                'total_debt' => $total_debt,
                'total_balance' => $total_balance,
                'total_sell_count' => $total_sell_count,
                'total_not_sell_count' => $total_not_sell_count

            ]
        );
    }


    public function stuns_statistic()
    {
        $request = request()->all();
        $startDate = $request['startDate'];
        $endDate = $request['endDate'];
        $type = $request['type'];
        $all = $this->stunsRepositories->stuns_list($startDate,$endDate);
        $data = $this->stunsRepositories->statistic_stuns_format_data($startDate, $endDate,$all, $type);
        return $this->success($data);
    }

    public function bookkeeping_statistic()
    {
        $request = request()->all();
        $startDate = $request['startDate'];
        $endDate = $request['endDate'];
        $all = $this->classify->withSum(['bookkeeping'=>function($query) use ($startDate,$endDate){
            $query->whereBetween('date', [$startDate, $endDate]);
        }],'money')->get();

        $data = $all->each(function ($item) {
            $item->bookkeeping_sum_money = (int)$item->bookkeeping_sum_money;
        });
        return $this->success($data);
    }

}
