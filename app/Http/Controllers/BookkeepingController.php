<?php

namespace App\Http\Controllers;

use App\Models\Bookkeeping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookkeepingController extends Controller
{
    use ApiResponse;

    private $bookkeeping, $paginate,$uid;

    public function __construct(Bookkeeping $Bookkeeping)
    {
        $this->bookkeeping = $Bookkeeping;
        $this->paginate = 10;


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->success($this->bookkeeping->user()->with('classify')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['uid'] = Auth::id();

        if ($this->bookkeeping->create($data)) {
           return $this->message('操作成功！');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = $this->bookkeeping->find($id);
        $this->success($detail);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = $this->bookkeeping->find($id);
        $this->success($detail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $bookkeeping = $this->bookkeeping->find($id);
        $bookkeeping->cid = $request->cid;
        $bookkeeping->money = $request->money;
        $bookkeeping->remarks = $request->remarks;
        $bookkeeping->date = $request->date;
        if ($bookkeeping->save()) {
            $this->message('编辑成功！');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->bookkeeping->destroy($id)) {
            $this->message('删除成功！');
        }
    }
}
