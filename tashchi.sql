-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 05, 2022 at 12:43 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tashchi`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookkeeping`
--

CREATE TABLE `bookkeeping` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `cid` tinyint(4) NOT NULL COMMENT '类型ID',
  `money` decimal(10,2) NOT NULL COMMENT '金额',
  `remarks` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '备注',
  `date` int(11) NOT NULL COMMENT '日期',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookkeeping`
--

INSERT INTO `bookkeeping` (`id`, `uid`, `cid`, `money`, `remarks`, `date`, `created_at`, `updated_at`) VALUES
(1, 8, 2, '100.00', 'tamq', 1644624000, 1644665635, 1644665635),
(2, 8, 2, '100.00', 'tamq', 1644624000, 1644665648, 1644665648),
(3, 8, 2, '4000.00', 'aaa', 1645142400, 1645524015, 1645524015),
(4, 8, 2, '100.00', 'aaa', 1644364800, 1645524037, 1645524037),
(5, 8, 3, '90.00', 'koloa', 1645660800, 1645587130, 1645587130),
(6, 8, 3, '1212.00', 'tame', 1648137600, 1646395281, 1646395281);

-- --------------------------------------------------------

--
-- Table structure for table `buy_repayment`
--

CREATE TABLE `buy_repayment` (
  `id` int(11) NOT NULL,
  `pid` tinyint(4) NOT NULL,
  `repayment_price` decimal(10,2) NOT NULL,
  `date_time` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buy_repayment`
--

INSERT INTO `buy_repayment` (`id`, `pid`, `repayment_price`, `date_time`, `updated_at`, `created_at`) VALUES
(21, 16, '1.00', 1645660800, 1645591128, 1645591128),
(7, 7, '1000.00', 1644624000, 1644586442, 1644586442),
(6, 6, '200.00', 1644451200, 1644586307, 1644586307),
(9, 9, '100.00', 1644710400, 1644641820, 1644641820),
(10, 10, '0.00', 1644710400, 1644641835, 1644641835),
(11, 11, '100.00', 1644710400, 1644641844, 1644641844),
(12, 12, '2000.00', 1644537600, 1644641853, 1644641853),
(13, 13, '503.00', 1644710400, 1644641867, 1644641867),
(14, 14, '200.00', 1644710400, 1644644675, 1644644675),
(15, 15, '5000.00', 1643846400, 1644646126, 1644646126),
(22, 17, '200.00', 1644854400, 1645600210, 1645600210),
(23, 18, '12.00', 1644854400, 1645600254, 1645600254),
(24, 19, '1.00', 1644940800, 1645600274, 1645600274),
(25, 20, '1.00', 1644940800, 1645600307, 1645600307);

-- --------------------------------------------------------

--
-- Table structure for table `classify`
--

CREATE TABLE `classify` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` int(11) NOT NULL COMMENT '用户Id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classify`
--

INSERT INTO `classify` (`id`, `uid`, `name`, `created_at`, `updated_at`) VALUES
(2, 1, '店铺', 1644649789, 1644649789),
(3, 2, '食品', 1644649789, 1644649789),
(4, 3, '其他', 1644649789, 1644649789);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(11, '2022_02_12_090645_create_bookkeeping_table', 2),
(12, '2022_02_12_092431_create_classify_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'user', 'c98d2a149adcd36c0bdd8251fa9167cb53bd1f11dff0c73e1bdb3c46ab285cf1', '[\"*\"]', NULL, '2022-02-02 21:51:04', '2022-02-02 21:51:04'),
(2, 'App\\Models\\User', 2, 'user', '6173eebb6a4e90208a2122a7364be577c0886cbe0a763d15fb346d1563257768', '[\"*\"]', NULL, '2022-02-02 21:51:38', '2022-02-02 21:51:38'),
(3, 'App\\Models\\User', 3, 'user', 'c16297b0acafd1d57227d6f494ce0a5b37c55f6923fd5ee9bd048eb16ef768b3', '[\"*\"]', NULL, '2022-02-02 21:52:51', '2022-02-02 21:52:51'),
(4, 'App\\Models\\User', 4, 'user', '0e31e41a1fbb3fecbd7cbec86f55c757e59d493c5ed9d5876bee539a3e1fa965', '[\"*\"]', NULL, '2022-02-02 21:52:59', '2022-02-02 21:52:59'),
(5, 'App\\Models\\User', 5, 'user', '6c6a4b852f57f20a7101469eb77aac304eee5da5372bde554c28ee7d07ae1115', '[\"*\"]', NULL, '2022-02-02 21:56:09', '2022-02-02 21:56:09'),
(6, 'App\\Models\\User', 6, 'user', '9f3e5d42b7fef1696527eb1f8f3576be39ec552b58c4876b6435742e61ec9b53', '[\"*\"]', NULL, '2022-02-02 21:58:57', '2022-02-02 21:58:57'),
(12, 'App\\Models\\User', 7, 'user', 'f2ec7772bb60dbd13500eaf73be7bfb9d808c0a9cc65440edede9dfa157f0d04', '[\"*\"]', NULL, '2022-02-03 00:40:31', '2022-02-03 00:40:31'),
(13, 'App\\Models\\User', 7, 'user', '199227607557791c3eebc7628982de0df352fbb9d21c568e12f6593a6da3c6e5', '[\"*\"]', NULL, '2022-02-03 00:41:29', '2022-02-03 00:41:29'),
(14, 'App\\Models\\User', 7, 'user', '02387411b3fd64a375c756600ae07532c7d246478c30288c5bf0d849d74f47f9', '[\"*\"]', NULL, '2022-02-03 00:41:36', '2022-02-03 00:41:36'),
(15, 'App\\Models\\User', 7, 'user', '15f193a9268428f03e2afbc00fa14f0f3bcabb4a3b22f68bed523943c34e031e', '[\"*\"]', NULL, '2022-02-03 00:41:42', '2022-02-03 00:41:42'),
(16, 'App\\Models\\User', 7, 'user', '5257216a28fa3c299138dd1e31bf3c896046eabf3509582bbd76674a711a6ef4', '[\"*\"]', '2022-02-03 00:44:41', '2022-02-03 00:42:27', '2022-02-03 00:44:41');

-- --------------------------------------------------------

--
-- Table structure for table `sell_repayment`
--

CREATE TABLE `sell_repayment` (
  `id` int(11) NOT NULL,
  `repayment_price` decimal(10,2) NOT NULL,
  `pid` int(11) NOT NULL,
  `date_time` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sell_repayment`
--

INSERT INTO `sell_repayment` (`id`, `repayment_price`, `pid`, `date_time`, `created_at`, `updated_at`) VALUES
(21, '4000.00', 8, 1644710400, 1644649789, 1644649789),
(22, '200.00', 6, 1646928000, 1646387379, 1646387379);

-- --------------------------------------------------------

--
-- Table structure for table `stuns`
--

CREATE TABLE `stuns` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '8' COMMENT '用户ID',
  `price` decimal(18,2) NOT NULL COMMENT '玉价格',
  `sell_price` decimal(10,2) DEFAULT NULL,
  `buy_name` varchar(80) DEFAULT NULL,
  `stun_count` smallint(6) NOT NULL COMMENT '玉数量',
  `price_image` varchar(255) NOT NULL COMMENT '玉图片',
  `borrow_flag` tinyint(4) DEFAULT NULL COMMENT '借入 1：nisiy amas 2:nisy',
  `lend_flag` tinyint(4) DEFAULT NULL COMMENT '借出 1:kerz ams 2:qerz',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态',
  `text` text,
  `sell_date` int(11) DEFAULT NULL COMMENT '卖时间',
  `date_time` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stuns`
--

INSERT INTO `stuns` (`id`, `uid`, `price`, `sell_price`, `buy_name`, `stun_count`, `price_image`, `borrow_flag`, `lend_flag`, `status`, `text`, `sell_date`, `date_time`, `updated_at`, `created_at`) VALUES
(8, 8, '3000.00', '10000.00', 'MAMAT', 1, 's:49:\"stuns/images/65ea2237f280b884e18e0b0df6c5b2f1.png\";', NULL, NULL, 2, NULL, NULL, 1644537600, 1644649789, 1644641807),
(6, 8, '10000.00', '20000.00', '马麦提', 1, 'N;', NULL, NULL, 2, NULL, NULL, 1644451200, 1646387379, 1644586307),
(7, 8, '2000.00', '0.00', '', 1, 'N;', NULL, NULL, 1, NULL, NULL, 1644624000, 1644649426, 1644586442),
(9, 8, '4000.00', '0.00', '', 1, 'N;', NULL, NULL, 1, NULL, NULL, 1644710400, 1644649414, 1644641820),
(10, 8, '2001.00', '0.00', '', 1, 'N;', NULL, NULL, 1, NULL, NULL, 1644796800, 1644649404, 1644641835),
(11, 8, '300.00', '0.00', '', 1, 'N;', NULL, NULL, 1, NULL, NULL, 1644710400, 1644649387, 1644641844),
(12, 8, '2000.00', '0.00', '', 1, 'N;', NULL, NULL, 1, NULL, NULL, 1644537600, 1644649372, 1644641853),
(13, 8, '1000.00', '0.00', '', 1, 'N;', NULL, NULL, 1, NULL, NULL, 1644710400, 1644649357, 1644641867),
(16, 8, '2.00', NULL, NULL, 1, 's:49:\"stuns/images/4ddf6067ae65d372a5f0985699675ec5.png\";', NULL, NULL, 1, NULL, NULL, 1645660800, 1645591128, 1645591128),
(14, 8, '1000.00', '0.00', '', 1, 'N;', NULL, NULL, 1, NULL, NULL, 1644710400, 1644649340, 1644644675),
(15, 8, '10000.00', '0.00', '', 1, 's:49:\"stuns/images/95f4de74cb614ccd91cbb656522e1069.jpg\";', NULL, NULL, 1, NULL, NULL, 1643846400, 1644649314, 1644646126),
(17, 8, '2000.00', NULL, NULL, 1, 's:49:\"stuns/images/6fc008ea3da8e9cc1fe6944bcd1d1c4f.jpg\";', NULL, NULL, 1, NULL, NULL, 1644854400, 1645600210, 1645600210),
(18, 8, '100.00', NULL, NULL, 1, 's:49:\"stuns/images/02bc9e6876baf80a5bb6a2de3aca9766.png\";', NULL, NULL, 1, NULL, NULL, 1644796800, 1645600254, 1645600254),
(19, 8, '200.00', NULL, NULL, 1, 's:49:\"stuns/images/515cb344abda885630192ccb71d4df52.png\";', NULL, NULL, 1, NULL, NULL, 1644940800, 1645600274, 1645600274),
(20, 8, '100.00', NULL, NULL, 1, 's:49:\"stuns/images/6cfd4776ebc79fa6bb828ffefa45ce6a.png\";', NULL, NULL, 1, NULL, NULL, 1644940800, 1645600307, 1645600307);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `account` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `account`, `name`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(7, 'heraa', 'abdusemet', '$2y$10$sAm/KnKnOfgVWozklpGxNu51CG0.D9xLaNyhep6xyFAQUSEFZaK0e', NULL, '2022-02-02 22:10:34', '2022-02-02 22:10:34'),
(8, 'ablat', 'abdusemet', '$2y$10$M5vwmgijY0j.2Vx/xBkFOuYqwLaWkWU8OG0tPB8bs0DyZrMw7Eh3m', NULL, '2022-02-03 01:28:54', '2022-02-03 01:28:54'),
(9, '123123', '123123', '$2y$10$TbhVNjMR.uMkpIqf6RaqDex3fPAUODA5KMncubhtwnhuoNHc4D.Fe', NULL, '2022-02-03 04:09:36', '2022-02-03 04:09:36'),
(10, '123123ll', '123123', '$2y$10$G3hExWxdkhImdXVHOGWlqOxbybgGV3tveYD5Jq.oAM1IijawtkqFW', NULL, '2022-02-03 05:15:14', '2022-02-03 05:15:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookkeeping`
--
ALTER TABLE `bookkeeping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buy_repayment`
--
ALTER TABLE `buy_repayment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classify`
--
ALTER TABLE `classify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `sell_repayment`
--
ALTER TABLE `sell_repayment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stuns`
--
ALTER TABLE `stuns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookkeeping`
--
ALTER TABLE `bookkeeping`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `buy_repayment`
--
ALTER TABLE `buy_repayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `classify`
--
ALTER TABLE `classify`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sell_repayment`
--
ALTER TABLE `sell_repayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `stuns`
--
ALTER TABLE `stuns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
