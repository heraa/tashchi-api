<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return 'Hello NSA!';
});
Route::group(['middleware' => 'auth:sanctum'], function () {
    //获取用户信息
    Route::get('currentUser', 'UserController@currentUser');
    //玉石管理
    Route::resource('stuns', 'StunsController');
    Route::resource('sell_repayment', 'SellRepaymentController');
    Route::resource('buy_repayment', 'BuyRepaymentController');
    //玉石上传图片
    Route::post('stuns/upload', "StunsController@upload");
    //记账
    Route::resource('bookkeeping', 'BookkeepingController');
    //分类
    Route::resource('classify', 'ClassifyController');
    //统计
    Route::get('statistic/total_stuns', 'StatisticController@stuns_total');
    //玉石统计
    Route::get('statistic/stuns_statistic', 'StatisticController@stuns_statistic');
    //记账统计
    Route::get('statistic/bookkeeping_statistic', 'StatisticController@bookkeeping_statistic');

});
require __DIR__ . '/auth.php';
